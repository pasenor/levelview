from PyQt4 import QtGui, QtCore
import pyqtgraph as pg
from numpy import array, float32, isinf, nan, asarray
import os

pg.setConfigOption("background", "w")

modeldict = {"Синергия": "Synergy",
             "7374": "7374",
             "Интеграл 2": "Integral",
             "Север": "Sever",
             "МТКП": "MTKP"
             }


class CanTxtSelector(QtGui.QDialog):
    def __init__(self):
        super().__init__()
        self.start = 0
        self.stop = 0
        self.canfile = ""
        self.calfile = ""
        self.configfile = ""
        self.outdfile = ""
        self.description = ""

        self.canInput = QtGui.QLineEdit()
        self.canBrowseBtn = QtGui.QPushButton("...")
        self.canBrowseBtn.setFixedWidth(25)
        canLabel = QtGui.QLabel("Файл CAN:")
        canLayout = QtGui.QHBoxLayout()
        for wdg in [canLabel, self.canInput, self.canBrowseBtn]:
            canLayout.addWidget(wdg)

        self.calInput = QtGui.QLineEdit()
        self.calBrowseBtn = QtGui.QPushButton("...")
        self.calBrowseBtn.setFixedWidth(25)
        calLabel = QtGui.QLabel("Файл калибровки:")
        calLayout = QtGui.QHBoxLayout()
        for wdg in [calLabel, self.calInput, self.calBrowseBtn]:
            calLayout.addWidget(wdg)

        self.confInput = QtGui.QLineEdit()
        self.confBrowseBtn = QtGui.QPushButton("...")
        self.confBrowseBtn.setFixedWidth(25)
        confLabel = QtGui.QLabel("Файл конфигурации:")
        confLayout = QtGui.QHBoxLayout()
        for wdg in [confLabel, self.confInput, self.confBrowseBtn]:
            confLayout.addWidget(wdg)

        modelLabel = QtGui.QLabel("Модель вагона:")
        self.modelComboBox = QtGui.QComboBox()
        self.modelComboBox.addItems(sorted([model for model in modeldict]))
        algoLabel = QtGui.QLabel("Алгоритм:")
        self.algoComboBox = QtGui.QComboBox()
        self.algoComboBox.addItems(["ServerLevel", "ABLevel"])

        modelLayout = QtGui.QHBoxLayout()
        for wdg in [modelLabel, self.modelComboBox,
                    algoLabel, self.algoComboBox]:
            modelLayout.addWidget(wdg)
        modelLayout.addStretch()

        descrLabel = QtGui.QLabel("Описание:")
        self.descrInput = QtGui.QLineEdit()
        descrLayout = QtGui.QHBoxLayout()
        descrLayout.addWidget(descrLabel)
        descrLayout.addWidget(self.descrInput)

        indexBox = QtGui.QGroupBox("индексы по синхропакетам")
        self.startInput = QtGui.QLineEdit()
        startLabel = QtGui.QLabel("от:")
        self.stopInput = QtGui.QLineEdit()
        stopLabel = QtGui.QLabel("до:")
        indexBtn = QtGui.QPushButton("Выбрать индексы")
        indexLayout = QtGui.QGridLayout()
        indexLayout.addWidget(startLabel, 0, 0, 1, 1)
        indexLayout.addWidget(self.startInput, 0, 1, 1, 1)
        indexLayout.addWidget(stopLabel, 0, 2, 1, 1)
        indexLayout.addWidget(self.stopInput, 0, 3, 1, 1)
        indexLayout.addWidget(indexBtn, 1, 1, 1, 2)
        indexBox.setLayout(indexLayout)

        buttonbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        buttonbox.addButton(QtGui.QPushButton("Отмена"), QtGui.QDialogButtonBox.RejectRole)

        resetBtn = QtGui.QPushButton("Сбросить")

        bboxlayout = QtGui.QHBoxLayout()
        bboxlayout.addWidget(resetBtn)
        bboxlayout.addStretch()
        bboxlayout.addWidget(buttonbox)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(canLayout)
        layout.addLayout(calLayout)
        layout.addLayout(confLayout)
        layout.addLayout(modelLayout)
        layout.addLayout(descrLayout)
        layout.addWidget(indexBox)
        layout.addLayout(bboxlayout)
        self.setLayout(layout)

        self.connect(indexBtn, QtCore.SIGNAL("clicked()"),
                     self.selectIndFromOutd)
        self.connect(self.canInput, QtCore.SIGNAL("editingFinished()"),
                     self.setCanFileFromInput)
        self.connect(self.canBrowseBtn, QtCore.SIGNAL("clicked()"),
                     self.setCanFileFromBtn)
        self.connect(self.confBrowseBtn, QtCore.SIGNAL("clicked()"),
                     self.setConfFileFromBtn)
        self.connect(self.calBrowseBtn, QtCore.SIGNAL("clicked()"),
                     self.setCalFileFromBtn)
        self.connect(buttonbox, QtCore.SIGNAL("accepted()"),
                     self, QtCore.SLOT("accept()"))
        self.connect(buttonbox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("reject()"))
        self.connect(self.descrInput, QtCore.SIGNAL("textChanged(QString)"),
                     self.setDescription)
        self.connect(resetBtn, QtCore.SIGNAL("clicked()"),
                     self.reset)

    def reset(self):
        self.start = 0
        self.startInput.setText("0")
        self.stop = 0
        self.stopInput.setText("0")
        self.canfile = ""
        self.canInput.setText("")
        self.calfile = ""
        self.calInput.setText("")
        self.configfile = ""
        self.confInput.setText("")
        self.outdfile = ""
        self.description = ""
        self.descrInput.setText("")


    def setDescription(self):
        self.description = self.descrInput.text()

    def guessModel(self):
        import codecs
        if self.configfile == "":
            return
        text = ""
        for encoding in ("utf-8", "cp1251"):
            try:
                with codecs.open(self.configfile, encoding=encoding) as f:
                    text = f.read(100)
                break
            except UnicodeDecodeError:
                continue

        if text == "":
            return

        if "Интеграл-М" in text:
            self.modelComboBox.setCurrentIndex(4)
        elif "Интеграл №" in text or "И Н Т Е Г Р А Л" in text:
            self.modelComboBox.setCurrentIndex(1)
        elif "MaxSem" in text:
            self.modelComboBox.setCurrentIndex(0)
        elif "С Е В Е Р" in text:
            self.modelComboBox.setCurrentIndex(3)
        elif "МТКП" in text:
            self.modelComboBox.setCurrentIndex(2)
        del text

    def selectIndFromOutd(self):
        indsel = IndexSelector(model=modeldict[self.modelComboBox.currentText()],
                               filename=self.guessOutTxt())
        if indsel.exec_():
            self.start = indsel.start
            self.stop = indsel.stop
            self.startInput.setText(str(indsel.start))
            self.stopInput.setText(str(indsel.stop))

    def guessOutTxt(self):
        if not self.configfile.endswith(".conf"):
            return ""
        root = os.path.dirname(self.configfile)
        filenames = ["out.txt", "outd.txt", "outf.txt"]
        for filename in filenames:
            fullname = "/".join((root, filename))
            if os.path.isfile(fullname):
                return fullname
        return ""

    def setCanFileFromInput(self):
        filename = self.canInput.text()
        if filename == "":
            return
        if os.path.isfile(filename) and filename.endswith("can.txt"):
            self.canfile = filename
            self.canInput.setStyleSheet("")
            root = filename[:filename.rfind("can.txt")]
            if self.configfile == "":
                configfile = root + "server.conf"
                if os.path.isfile(configfile):
                    self.confInput.setText(configfile)
                    self.setConfFileFromInput()
            if self.calfile == "":
                calfile = root + "server.cal"
                if os.path.isfile(calfile):
                    self.calInput.setText(calfile)
                    self.setCalFileFromInput()
        else:
            self.canInput.setStyleSheet("background-color: red")

    def setCanFileFromBtn(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Добавить CAN файл",
                                                     ".", "CAN файлы (can.txt)")
        self.canInput.setText(filename)
        self.descrInput.setText(os.path.split(os.path.dirname(filename))[-1])
        self.setCanFileFromInput()

    def setCalFileFromInput(self):
        filename = self.calInput.text()
        if filename == "":
            return
        if os.path.isfile(filename) and filename.endswith(".cal"):
            self.calfile = filename
            self.calInput.setStyleSheet("")
        else:
            self.calInput.setStyleSheet("background-color: red")

    def setCalFileFromBtn(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Добавить файл калибровки",
                                                     ".", "файлы калибровки (*.cal)")
        self.calInput.setText(filename)
        self.setCalFileFromInput()

    def setConfFileFromInput(self):
        filename = self.confInput.text()
        if filename == "":
            return
        if os.path.isfile(filename) and filename.endswith(".conf"):
            self.configfile = filename
            self.confInput.setStyleSheet("")
            self.guessModel()
            self.descrInput.setText(self.modelComboBox.currentText() + ", " + self.descrInput.text())
        else:
            self.confInput.setStyleSheet("background-color: red")

    def setConfFileFromBtn(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Добавить файл конфигурации",
                                                     ".", "файлы конфигурации (*.conf)")
        self.confInput.setText(filename)
        self.setConfFileFromInput()


    def getModel(self):
        return modeldict[self.modelComboBox.currentText()]

    def getAlgorithm(self):
        return self.algoComboBox.currentText()

levelColDict = {
    "Synergy": 5,
    "Integral": 5,
    "Sever": 5,
    "MTKP": 5,
    "7374": 5
}


class GrafikWidget(pg.PlotWidget):
    def __init__(self):
        super().__init__()
        self.selectOn = False
        self.trackMouse = False
        self.start = 0
        self.stop = 0
        self.region = pg.LinearRegionItem()
        self.pi = self.plot(pen="b")

    def toggleSelect(self):
        self.selectOn = not self.selectOn

    def mousePressEvent(self, event):
        if self.selectOn:
            self.plotItem.vb.setMouseEnabled(x=False, y=False)
            self.trackMouse = True
            self.start = int(self.plotItem.vb.mapSceneToView(event.pos()).x())
            self.stop = self.start
            self.plotItem.removeItem(self.region)
            self.plotItem.addItem(self.region)
            self.region.setRegion((self.start, self.stop))
            return
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if self.selectOn and self.trackMouse:
            x = int(self.plotItem.vb.mapSceneToView(event.pos()).x())
            self.start = min(self.start, x)
            self.stop = max(self.stop, x)
            self.region.setRegion((self.start, self.stop))
            return
        else:
            super().mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        if self.selectOn:
            self.trackMouse = False
            self.plotItem.vb.setMouseEnabled(x=True, y=True)
            self.plotItem.vb.setMouseMode(pg.ViewBox.PanMode)
            self.emit(QtCore.SIGNAL("rangeChanged"), (self.start, self.stop))
        else:
            super().mouseReleaseEvent(event)

def guessNLines(filename):
    import os
    with open(filename) as f:
        nchars = 0
        for nlines, line in enumerate(f):
            if nlines > 10:
                break
            nchars += len(line)
        avgLine = nchars / nlines
        return os.path.getsize(filename) / avgLine


class FileOpenThread(QtCore.QThread):
    import os
    def __init__(self):
        super().__init__()
        self.filename = ""
        self.level = None

    def setFileName(self, filename):
        self.filename = filename
    def setModel(self, model):
        self.model = model

    def run(self):
        if os.path.isfile(self.filename):
            self.emit(QtCore.SIGNAL("fileNotFound"))
        N = guessNLines(self.filename)
        linesChunk = max(1, N // 100)
        with open(self.filename) as f:
            i = levelColDict[self.model]
            level = []
            for lineCnt, line in enumerate(f):
                try:
                    level.append(line.split()[i])
                except IndexError:
                    print("could not parse line {0} in {1}".format(lineCnt+1, self.filename))
                    continue
                if lineCnt % linesChunk == 0:
                    self.emit(QtCore.SIGNAL("progress"), lineCnt // linesChunk)
            self.level = asarray(level, dtype=float32)
            del level
            #self.level[isinf(level)] = nan
        self.emit(QtCore.SIGNAL("done"))


class IndexSelector(QtGui.QDialog):
    def __init__(self, filename="", model="Synergy"):
        super().__init__()
        self.start = 0
        self.stop = 0
        self.model = model
        self.indSetSignal = QtCore.SIGNAL("indicesSelected")

        self.filenameLabel = QtGui.QLabel("файл out*.txt:")
        self.filenameInput = QtGui.QLineEdit()
        self.filenameInput.setText(filename)
        self.filenameBrowseBtn = QtGui.QPushButton("...")
        self.filenameBrowseBtn.setFixedWidth(25)
        readBtn = QtGui.QPushButton("Обновить")

        filenameLayout = QtGui.QHBoxLayout()
        for wdg in [self.filenameLabel, self.filenameInput, self.filenameBrowseBtn,
                    readBtn]:
            filenameLayout.addWidget(wdg)

        self.grafik = GrafikWidget()
        # self.grafik = p.plotItem

        self.okBtn = QtGui.QPushButton("OK")
        self.fromInput = QtGui.QLineEdit()
        self.fromInput.setValidator(QtGui.QIntValidator(self.start, self.stop))
        self.fromInput.setText(str(self.start))
        fromLabel = QtGui.QLabel("от:")
        fromLabel.setBuddy(self.fromInput)

        self.toInput = QtGui.QLineEdit()
        self.toInput.setValidator(QtGui.QIntValidator(self.start, self.stop))
        self.toInput.setText(str(self.stop ))
        toLabel = QtGui.QLabel("до:")
        toLabel.setBuddy(self.toInput)

        selectBtn = QtGui.QToolButton()
        selectBtn.setText("Выделение")
        selectBtn.setCheckable(True)
        selectBtn.setChecked(False)
        selectBtn.setFixedSize(selectBtn.sizeHint())

        self.progressStack = QtGui.QStackedWidget()
        self.progressStack.setFixedHeight(selectBtn.height() * 0.9)
        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setFixedHeight(self.progressStack.height())
        self.progressStack.addWidget(QtGui.QWidget())
        self.progressStack.addWidget(self.progressBar)

        fromToLayout = QtGui.QHBoxLayout()
        for w in [fromLabel, self.fromInput, toLabel,
                  self.toInput, selectBtn, self.progressStack]:
            fromToLayout.addWidget(w)
        fromToLayout.addStretch()
        layout = QtGui.QGridLayout()
        layout.addLayout(filenameLayout, 0, 0, 1, 4)
        layout.addLayout(fromToLayout, 1, 0, 1, 4)
        layout.addWidget(self.grafik, 2, 0, 3, 4)
        layout.addWidget(self.okBtn, 5, 3, 1, 1)
        self.setLayout(layout)

        self.connect(self.fromInput, QtCore.SIGNAL("editingFinished()"),
                     self.setStart)
        self.connect(self.toInput, QtCore.SIGNAL("editingFinished()"),
                     self.setStop)
        self.connect(self.okBtn, QtCore.SIGNAL("clicked()"),
                     self, QtCore.SLOT("accept()"))
        self.connect(self.filenameBrowseBtn, QtCore.SIGNAL("clicked()"),
                     self.setFileFromDlg)
        self.connect(readBtn, QtCore.SIGNAL("clicked()"),
                     self.readLevel)

        self.connect(selectBtn, QtCore.SIGNAL("clicked()"),
                     self.onSelectToggled)
        self.connect(self.grafik, QtCore.SIGNAL("rangeChanged"),
                     self.onRangeChanged)

        self.levelReadThread = FileOpenThread()
        self.connect(self.levelReadThread, QtCore.SIGNAL("progress"),
                     self.onLevelReadProgress)
        self.connect(self.levelReadThread, QtCore.SIGNAL("done"),
                     self.onDoneReadingLevel)
        self.readLevel()

    def onSelectToggled(self):
        if self.grafik.selectOn:
            self.grafik.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        else:
            self.grafik.setCursor(QtGui.QCursor(QtCore.Qt.CrossCursor))
        self.grafik.toggleSelect()

    def onRangeChanged(self, rng):
        self.setRange(rng)

    def readLevel(self):
        self.progressStack.setCurrentIndex(1)
        self.levelReadThread.setFileName(self.filenameInput.text())
        self.levelReadThread.setModel(self.model)
        self.levelReadThread.start()

    def onDoneReadingLevel(self):
        self.progressStack.setCurrentIndex(0)
        self.grafik.pi.setData(self.levelReadThread.level)
        self.start = len(self.levelReadThread.level)//5
        self.stop = len(self.levelReadThread.level) - self.start
        self.levelReadThread.level = None

    def onLevelReadProgress(self, percentage):
        self.progressBar.setValue(percentage)

    def setFileFromDlg(self):
        import os
        root = self.filenameInput.text()
        if os.path.isfile(root):
            root = root[:root.rfind("/")]
        else:
            root = "."
        filename = QtGui.QFileDialog.getOpenFileName(self, "файл сигнала",
                                                     root, "Файлы сигналов (out*.txt);;Все файлы (*)")
        self.filenameInput.setText(filename)

    def setRange(self, rng):
        self.start, self.stop = rng
        self.fromInput.setText(str(self.start))
        self.toInput.setText(str(self.stop))

    def setStart(self):
        self.start = int(self.fromInput.text())
        self.toInput.validator().setBottom(self.start)
        self.grafik.region.setRegion([self.start, self.stop])

    def setStop(self):
        self.stop = int(self.toInput.text())
        self.fromInput.validator().setTop(self.stop)
        self.grafik.region.setRegion([self.start, self.stop])

    def AllSet(self):
        self.emit(self.indSetSignal, (self.start, self.stop))
        self.close()

if __name__ == "__main__":
    from numpy import *
    import sys
    app = QtGui.QApplication(sys.argv)
    selector = CanTxtSelector()
    selector.show()
    app.exec_()

