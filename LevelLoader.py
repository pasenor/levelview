# coding: utf-8
import can_txt2
import calc2 as calc
from xml.etree import ElementTree
from PyQt4 import QtGui, QtCore
import os


class LevelLoader:
    def __init__(self):
        self.levels = []
        self.ccc = []
        self.labels = []
        self.tree = None
        self.proezdlist = []

    def loadXML(self, xmlfile):
        import calc2

        self.tree = ElementTree.parse(xmlfile)
        root = self.tree.getroot()
        proezdy = root.findall("proezd")
        for proezd in proezdy:
            if proezd is None:
                raise RuntimeError("cannot parse xml")

            arguments = {}
            try:
                arguments["label"] = proezd.find("label").text
                arguments["cantxt"] = proezd.find("cantxt").text
                arguments["conf"] = proezd.find("conf").text
                arguments["cal"] = proezd.find("cal").text
                arguments["model"] = proezd.find("model").text
                arguments["algorithm"] = proezd.find("algorithm").text
                arguments["start"] = int(eval(proezd.find("start").text))
                arguments["stop"] = int(eval(proezd.find("stop").text))
            except:
                raise RuntimeError("cannot parse xml")

            self.addProezd(arguments)

    def addProezd(self, argdict):
        busyBar = BusyBar(argdict)
        busyBar.exec_()

        if self.tree is not None:
            root = self.tree.getroot()
        else:
            root = ElementTree.Element("data")
            self.tree = ElementTree.ElementTree(root)

        self.labels.append(busyBar.output["labeltext"])
        self.ccc.append(busyBar.output["ccc"])
        self.proezdlist.append(busyBar.output["proezd"])
        root.append(busyBar.output["proezd"])
        return busyBar.output["calculator"], busyBar.output["proezd"]

    def getLevels(self):
        calculators = []
        levels = []
        for ccc in self.ccc:
            can, configfile, calfile, model, algorithm = ccc
            ca = calc.ConfigAdapter(configfile, calfile, model=model)
            calculator = calc.select_calculator(algorithm)(can, ca)
            calcres = calculator()
            levels.append(calcres.level)
            calculators.append(calculator)
        return calculators, levels, self.labels, self.proezdlist


class FileOpenThread(QtCore.QThread):
    def __init__(self):
        super().__init__()
        self.argdict = {}
        self.labeltext = ""
        self.ccc = None
        self.proezd = None
        self.calculator = None

    def setArgs(self, argdict):
        self.argdict = argdict

    def run(self):
        label = self.argdict["label"]
        if label == "":
            label = self.argdict["cantxt"].replace('/', os.path.sep) + "\n" + "индексы: {0} - {1}".format(self.argdict["start"],
                                                                                self.argdict["stop"])
        self.labeltext = label + "\nалгоритм: " + self.argdict["algorithm"]
        start, stop = can_txt2.lines_range(self.argdict["cantxt"], self.argdict["start"], self.argdict["stop"],
                                           onProgress=lambda x: self.emit(QtCore.SIGNAL("progress"), int(x * 0.3)))
        ca = calc.ConfigAdapter(self.argdict["conf"], self.argdict["cal"], model=self.argdict["model"])
        try:
            can = can_txt2.CanTxt(self.argdict["cantxt"], confAdapter=ca,
                              onProgress=lambda x: self.emit(QtCore.SIGNAL("progress"), int(x * 0.7) + 30),
                              start=start, stop=stop, model=self.argdict["model"])
            if len(can.t_gyro) == 0:
                raise RuntimeError("ERROR: empty gyro signal in {0}".format(self.argdict["cantxt"]))
            if len(can.t_synchro) == 0:
                raise RuntimeError("ERROR: empty synchro signal in {0}".format(self.argdict["cantxt"]))
        except RuntimeError as err:
            self.emit(QtCore.SIGNAL("error"), str(err))

        self.ccc = (can, self.argdict["conf"], self.argdict["cal"],
                    self.argdict["model"], self.argdict["algorithm"])
        self.calculator = calc.select_calculator(self.argdict["algorithm"])(can, ca)
        self.proezd = ElementTree.Element("proezd")
        for key in sorted(self.argdict.keys()):
            ElementTree.SubElement(self.proezd, key).text = str(self.argdict[key])

        self.emit(QtCore.SIGNAL("done"))
        self.quit()


class BusyBar(QtGui.QDialog):
    fileOpenThread = FileOpenThread()

    def __init__(self, argdict):
        super().__init__()
        self.output = None

        label = QtGui.QLabel("Открываю {0}...".format(argdict["cantxt"]))
        self.bar = QtGui.QProgressBar()
        self.bar.setFixedHeight(self.bar.sizeHint().height())
        self.bar.setFixedWidth(self.bar.sizeHint().width() * 3)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(self.bar)
        self.setLayout(layout)

        self.connect(self.fileOpenThread, QtCore.SIGNAL("progress"),
                     self.onProgress)
        self.connect(self.fileOpenThread, QtCore.SIGNAL("done"),
                     self.onDoneReading)
        self.connect(self.fileOpenThread, QtCore.SIGNAL("error"),
                     self.onError)

        self.fileOpenThread.setArgs(argdict)
        self.fileOpenThread.start()

    def onProgress(self, x):
        self.bar.setValue(x)

    def onError(self, msg):
        mbox = QtGui.QErrorMessage()
        mbox.setWindowTitle("Error while parsing can.txt")
        self.hide()
        mbox.showMessage(msg)
        mbox.exec_()
        self.close()

    def onDoneReading(self):
        self.bar.setValue(100)
        self.output = {
            "labeltext": self.fileOpenThread.labeltext,
            "ccc": self.fileOpenThread.ccc,
            "proezd": self.fileOpenThread.proezd,
            "calculator": self.fileOpenThread.calculator
        }
        self.close()


def guessNLines(filename):
    import os

    with open(filename) as f:
        nchars = 0
        for nlines, line in enumerate(f):
            if nlines > 10:
                break
            nchars += len(line)
        avgLine = nchars / nlines
        return os.path.getsize(filename) / avgLine
