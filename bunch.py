class Bunch:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return repr(self.__dict__)

    def __getitem__(self, item):
        if str(item)[0].isnumeric():
            item = "i"+str(item)
        return getattr(self, item)

def valid(key):
    if key[0].isnumeric():
        return False
    allowed = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_')
    if not set(key).issubset(allowed):
        return False
    return True

def transform_key(key):
    transformed = []
    allowed = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_')
    if key[0].isnumeric():
        transformed.append('i')
    for c in key:
        if c in allowed:
            transformed.append(c)
        else:
            transformed.append('_')
    return "".join(transformed)


def bunch_from_dict(d):
    if type(d) is not dict:
        return d
    bunch = Bunch()
    bunch.__dict__.update(d)
    keys = list(bunch.__dict__.keys())
    for key in keys:
        newkey = transform_key(str(key))
        bunch.__dict__[newkey] = bunch_from_dict(bunch.__dict__[key])
        if newkey != key:
            bunch.__dict__.pop(key)
    return bunch

class PyrailBunch:

    '''
    Что-то вроде анонимного класса.

    Пример использования:
    point2d = Bunch(x=1, y=2)
    point3d = Bunch(x=1, y=2, z=3)

    Ссылки:
    http://stackoverflow.com/questions/1123000/does-python-have-anonymous-classes
    http://code.activestate.com/recipes/52308/

    '''

    def __init__(self, **kwds):
        self.__dict__.update(kwds)


    #  Ниже идут методы, позволяющие работать с объектом класса как со словарём:

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return self.__dict__.__str__()

    def __repr__(self):
        return self.__dict__.__repr__()

    def __len__(self):
        return self.__dict__.__len__()

    def __getitem__(self, key):
        return self.__dict__.__getitem__(key)

    def __iter__(self):
        return self.__dict__.__iter__()

    def __contains__(self, item):
        return self.__dict__.__contains__(item)

    def __setitem__(self, key, value):
        return self.__dict__.__setitem__(key, value)

    def __delitem__(self, key):
        self.__dict__.__delitem__(key)
