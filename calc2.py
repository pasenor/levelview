from numpy import pi, empty, zeros, diff, cumsum, hamming, convolve, arange, median, where
from scipy.interpolate import interp1d as interp
from PyQt4 import QtGui, QtCore


def interp1d(t, x):
    return interp(t, x, bounds_error=False, fill_value=0, kind="nearest")

import server_conf as sc
from numpy import roll
#from pyrail.common.diff import win_diff
from configparams import configparams
from bunch import bunch_from_dict

def gauss_smooth(y, iwin, fwhm):
    if iwin == 0 or iwin * fwhm < 1:
        return y
    from scipy.ndimage.filters import gaussian_filter1d
    from numpy import log, sqrt
    sigma = fwhm*iwin*0.5/sqrt(2*log(2))
    return gaussian_filter1d(y, sigma)

def shift(x, n):
    return roll(x, -int(n))


def alpha_beta(gyr, acs, alpha, beta, dt=1e-2):
    gyr_corr = zeros(len(gyr))
    gyr_angle = zeros(len(gyr))

    gyr_corr[0] = gyr[0]

    for i in range(1, len(gyr)):
        predicted = gyr_angle[i - 1] + gyr_corr[i - 1] * dt
        measured = acs[i]
        residual = max(1e-8, measured - predicted)
        gyr_angle[i] = predicted + alpha * residual
        gyr_corr[i] = gyr[i] + beta / dt * residual

    return gyr_angle


def derivative(x, window=100):
    from commonmath import win_diff

    return win_diff(x, window) / window


def value_getter_scalar(func):
    def wrapper(self):
        varname = "_" + func.__name__
        try:
            return getattr(self, varname)
        except AttributeError:
            value = func(self)
            self.__dict__[varname] = value
            return value

    return wrapper


def value_getter_vector(func):
    def wrapper(self, i):
        varname = "_" + func.__name__
        try:
            return getattr(self, varname)[i]
        except AttributeError:
            value = func(self, i)
            self.__dict__[varname] = {i: value}
            return value
        except KeyError:
            value = func(self, i)
            self.__dict__[varname][i] = value
            return value

    return wrapper


class ConfigAdapter:
    def __init__(self, conffilename, calfilename, model):
        self.params = bunch_from_dict(configparams[model])
        self.model = model
        self.conffilename = conffilename
        self.calfilename = calfilename

        self.conf = sc.load_server_conf(conffilename, key_mode='lower')
        self.cal = sc.load_server_cal(calfilename, key_mode='lower')

        self._gyro_coeff = [None, None, None]
        self._input_coeff = self.conf.nodes["lir.inputcoef"]

        self.shift_getter = None
        self.set_direction("fwd")
        self.dircoeff = 1

    def set_direction(self, direction):
        if hasattr(self.conf.system, "extradelayfwd"):
            if direction == "fwd":
                self.dircoeff = -1
                self.shift_getter = lambda name: self.conf.system.extradelayfwd[self.params.inds[name]]
            else:
                self.dircoeff = 1
                self.shift_getter = lambda name: self.conf.system.extradelayrev[self.params.inds[name]]
        else:
            self.dircoeff = -1 if direction == "fwd" else 1
            self.shift_getter = lambda name: self.conf.system.extradelay[self.params.inds[name]]

    def flip_direction(self):
        if self.dircoeff == 1:
            self.set_direction("fwd")
        else:
            self.set_direction("rev")

    @value_getter_scalar
    def acs_val_shift(self):
        try:
            return self.cal["acs0"]
        except KeyError:
            return self.cal["acs1"]

    @value_getter_scalar
    def gyr_angle_smd(self):
        return self.conf.system.filtering[self.params.inds.gyr_angle]

    @value_getter_scalar
    def gyr_sm_smd(self):
        return self.conf.system.filtering[self.params.inds.gyr_sm]

    @value_getter_scalar
    def speed_corr_smd(self):
        return self.conf.system.filtering[self.params.inds.speed_corr]

    @value_getter_scalar
    def acs_smd(self):
        return self.conf.system.filtering[self.params.inds.acs]

    @value_getter_scalar
    def alpha(self):
        return 1e-4

    @value_getter_scalar
    def beta(self):
        return 1e-4

    @value_getter_vector
    def z_base(self, i):
        return self.cal.n[i]

    @value_getter_vector
    def z_coeff(self, i):
        return self.conf.coefficients.zcoeff[i]

    def input_coeff(self, i):
        return self._input_coeff[i]

    @value_getter_vector
    def z_smd(self, i):
        return self.conf.system.filtering[self.params["inds"]["z"][i]]

    def z_coeff_full(self, i):
        res = 1 << self.params["lir_bits"][i]
        result = 2 * pi * self.params["lir_radius"][i] / res * self.input_coeff(i) * self.z_coeff(i)
        return result if i < 4 else -result

    def dir_event(self):
        if self.model != "7374":
            try:
                node_id, mask, mode = self.conf.events["8"]
                node_id |= 0x280
                return node_id, mask, mode
            except KeyError:
                return 0, 0, 0
        else:
            return 0x28F, 0, 0



    # @value_getter_vector
    # def z_shift(self, i):
    # return self.conf.system.extradelayfwd[self.params["inds"]["z"][i]]

    @value_getter_scalar
    def dl(self):
        return self.cal.dx

    @value_getter_scalar
    def kdz(self):
        return self.conf.coefficients.kdz

    def gyro_calibr_coeff(self, i):
        if self._gyro_coeff[i] is None:
            self._gyro_coeff[i] = self.conf.calibration['gyro{0}'.format(i)][1]
        if i == 0:
            return self._gyro_coeff[i] * self.hi_freq_coeff()
        elif i == 1:
            return self._gyro_coeff[i] * self.speed_corr_coeff()
        else:
            return self._gyro_coeff[i]

    @value_getter_vector
    def gyro_base(self, i):
        return self.cal.gs[i]

    @value_getter_vector
    def acs_calibr_coeff(self, i):
        return self.conf.calibration["acs{0}".format(i)][1] * self.acs_coeff()

    @value_getter_scalar
    def acs_coeff(self):
        return self.conf.coefficients.acscoeff

    @value_getter_vector
    def acs_base(self, i):
        return self.conf.calibration["acs{0}".format(i)][0]

    @value_getter_scalar
    def hi_freq_coeff(self):
        return self.conf.coefficients.hifreqcoeff

    @value_getter_scalar
    def speed_corr_coeff(self):
        return self.conf.coefficients.speedcorrcoeff

    @value_getter_scalar
    def gyro_shift(self):
        return self.shift_getter("gyr_angle")

    @value_getter_scalar
    def acs_shift(self):
        return self.shift_getter("acs")

    @value_getter_scalar
    def speed_shift(self):
        return self.shift_getter("speed")

    @value_getter_scalar
    def z_shift(self):
        return self.shift_getter("z")[1]

    @value_getter_scalar
    def speed_corr_shift(self):
        return self.shift_getter("speed_corr")


def generate_config(adapter):
    with open(adapter.conffilename) as f:
        configlines = [l.strip() for l in f.readlines()]
    configdict = {}
    for i, l in enumerate(configlines):
        if l.startswith(";") or len(l) == 0 or l.startswith("["):
            continue
        configdict[l[:l.find("=")].strip()] = i

    def set_field(name, value):
        line_index = configdict[name]
        line = configlines[line_index]
        try:
            valstring = ",".join(value)
        except TypeError:
            valstring = str(value)
        line = line[:line.find("=") + 1] + valstring
        configlines[line_index] = line

    def set_within_field(name, index, value):
        line_index = configdict[name]
        line = configlines[line_index]
        eq_index = line.find("=")
        data = line[eq_index:].split(",")
        data[index] = str(value)
        set_field(name, data)

    set_field("LIR.Bits", adapter.params["lir_bits"][0])
    for i in adapter.params.inds.z:
        set_within_field("Filtering", i, adapter.z_smd(i - adapter.params.inds.z[0]))
    for paramname in adapter.params.inds.__dict__:
        if paramname == "z":
            continue
        try:
            index = adapter.params.inds.__dict__[paramname]
            set_within_field("Filtering", index, getattr(adapter, paramname + "_smd")())
        except AttributeError:
            pass
    set_field("AcsCoeff", adapter.acs_coeff())
    set_field("KdZ", adapter.kdz())
    set_field("HiFreqCoeff", adapter.hi_freq_coeff())
    set_field("SpeedCorrCoeff", adapter.speed_corr_coeff())
    set_field("AcsCoeff", adapter.acs_coeff())

    return "\n".join(configlines)


class LevelCalc:
    def __init__(self, can, configAdapter, synchrostart=0, synchrostop=None):
        self.conf = configAdapter
        self.can = can
        self.g = 1/0.08 if self.conf.model == "7374" else 9.8
        self.conf.set_direction("fwd" if median(self.can.dircoeff) == 1 else "rev")
        self.JUMPVAL = 0.005 # Gyro jump threshold

    def __norm__(self, i, j, bits):
        base = 1 << (bits - 1)
        mask = base * 2 - 1
        a = base - i
        b = (j + a) & mask
        return b - base

    def flip_direction(self):
        self.conf.flip_direction()

    def __call__(self):

        z = {}
        for i, zraw in self.can.z.items():
            z[i] = self.__norm__(zraw, self.conf.z_base(i),
                                 self.conf.params["lir_bits"][i]) * self.conf.z_coeff_full(i)
        for i in z:
            z[i] = gauss_smooth(z[i], self.conf.z_smd(i), 0.5)
            z[i] = shift(z[i], self.conf.z_shift())

        dz = {}
        for i in z:
            if i < 4:
                dz[i] = (z[i + 4] - z[i]) * self.conf.kdz()

        gyro = {}
        for i in self.can.gyro:
            gyro[i] = (self.can.gyro[i] - self.conf.gyro_base(i)) * self.conf.gyro_calibr_coeff(i)

        acs = {}
        for i in self.can.acs:
            acs[i] = (self.can.acs[i] - self.conf.acs_base(i)) \
                     * self.conf.acs_calibr_coeff(i) - self.conf.acs_val_shift()

        speed = self.conf.dl() / derivative(self.can.t_synchro) * 1e6 * self.conf.dircoeff
        win_speed = hamming(75)
        win_speed /= sum(win_speed)
        speed = convolve(speed, win_speed, 'same')

        t = self.can.t_synchro
        speed = speed
        speed_shifted = shift(speed, self.conf.speed_shift())

        for i in dz:
            dz[i] = dz[i]
        gyro_synced = {}
        acs_synced = {}
        acs_synced_shifted = {}
        for i in gyro:
            spline = interp1d(self.can.t_gyro, gyro[i])
            gyro_synced[i] = spline(t)
        for i in acs:
            spline = interp1d(self.can.t_gyro, acs[i])
            acs_synced[i] = spline(t)
            acs_synced[i] = gauss_smooth(acs_synced[i], self.conf.acs_smd(), 0.5)
            acs_synced_shifted[i] = shift(acs_synced[i], self.conf.acs_shift())

        gyr_angle = cumsum(gyro[0]) * 0.01
        spline = interp1d(self.can.t_gyro, gyr_angle)

        gyr_angle = spline(t)
        gyr_angle = gauss_smooth(gyr_angle, self.conf.gyr_angle_smd(), 0.5)
        jumps = where(abs(diff(gyr_angle)) > self.JUMPVAL)[0]
        for jmp in jumps:
            gyr_angle[jmp+1:] -= gyr_angle[jmp+1] - gyr_angle[jmp]
        gyr_sm = gauss_smooth(gyr_angle, self.conf.gyr_sm_smd(), 0.5)
        hi_freq = gyr_angle - gyr_sm
        hi_freq_shifted = shift(hi_freq, self.conf.gyro_shift())

        speed_corr = gyro_synced[1] * speed_shifted / self.g
        speed_corr = gauss_smooth(speed_corr, self.conf.speed_corr_smd(), 0.5)
        speed_corr_shifted = shift(speed_corr, self.conf.speed_corr_shift())

        acs_corr_mm = (acs_synced_shifted[0] + speed_corr_shifted) * 1600
        hi_freq_mm = hi_freq_shifted * 1600

        level = dz[0] + acs_corr_mm + hi_freq_mm
        coord = arange(len(level)) * self.conf.dl()

        return bunch_from_dict(
            dict(
                dz=dz,
                z=z,
                gyro=gyro,
                acs=acs_synced[0],
                speed=speed,
                speed_corr=speed_corr,
                acs_corr_mm=acs_corr_mm,
                hi_freq_mm=hi_freq_mm,
                gyr_angle=gyr_angle,
                gyr_sm=gyr_sm,
                level=level,
                coord=coord))


class LevelCalcAB:
    def __init__(self, can, configAdapter, synchrostart=0, synchrostop=None):
        self.conf = configAdapter
        self.can = can
        self.g = 1 / 0.08 if self.conf.model == "7374" else 9.8

    def __norm__(self, i, j, bits):
        base = 1 << (bits - 1)
        mask = base * 2 - 1
        a = base - i
        b = (j + a) & mask
        return b - base

    def __call__(self):
        z = {}
        for i, zraw in self.can.z.items():
            z[i] = self.__norm__(zraw, self.conf.z_base(i),
                                 self.conf.params["lir_bits"][i]) * self.conf.z_coeff_full(i)
        for i in z:
            z[i] = gauss_smooth(z[i], self.conf.z_smd(i), 0.5)
            z[i] = shift(z[i], self.conf.z_shift())

        dz = {}
        dz_shifted = {}
        for i in z:
            if i < 4:
                dz[i] = (z[i + 4] - z[i]) * self.conf.kdz()

        gyro = {}
        for i in self.can.gyro:
            gyro[i] = (self.can.gyro[i] - self.conf.gyro_base(i)) * self.conf.gyro_calibr_coeff(i)

        acs = {}
        for i in self.can.acs:
            acs[i] = (self.can.acs[i] - self.conf.acs_base(i)) \
                     * self.conf.acs_calibr_coeff(i) - self.conf.acs_val_shift()

        speed = self.conf.dl() / derivative(self.can.t_synchro) * 1e6
        win_speed = hamming(75)
        win_speed /= sum(win_speed)
        speed = convolve(speed, win_speed, 'same')

        t = self.can.t_synchro
        speed = speed

        for i in dz:
            dz[i] = dz[i]
            dz_shifted[i] = shift(dz[i], self.conf.z_shift())

        gyro_synced = {}
        gyro_synced_shifted = {}
        for i in gyro:
            spline = interp1d(self.can.t_gyro, gyro[i])
            gyro_synced[i] = spline(t)
            gyro_synced[i] = gauss_smooth(gyro_synced[i], self.conf.gyr_angle_smd(), 0.5)
            gyro_synced_shifted[i] = shift(gyro_synced[i], self.conf.gyro_shift())

        spline = interp1d(t, speed)
        speed_gyro = spline(self.can.t_gyro)
        speed_corr = gyro[1] * speed_gyro / self.g
        speed_corr_shifted = shift(speed_corr, self.conf.speed_corr_shift())
        gyrcorr = alpha_beta(gyro[0], acs[0] + speed_corr_shifted, 0, self.conf.alpha(), self.conf.beta())
        spline = interp1d(self.can.t_gyro, gyrcorr)
        gyrcorr_synced = spline(t)
        gyrcorr_synced_shifted = shift(gyrcorr_synced, self.conf.gyro_shift())

        level = dz_shifted[1] + gyrcorr_synced_shifted * 1600
        coord = arange(len(level)) * self.conf.dl()

        return bunch_from_dict(
            dict(
                dz=dz,
                z=z,
                gyro=gyro,
                speed=speed,
                speed_corr=speed_corr,
                level=level,
                coord=coord))


def select_calculator(algorithm):
    calculators = {"ServerLevel": LevelCalc,
                   "ABLevel": LevelCalcAB}
    return calculators[algorithm]


if __name__ == "__main__":
    import can_txt2
    import os
    # from matplotlib import pyplot as plt

    root = r"\\proezd\proezd\RPO-1\Проезды\Metro Msk Sinergy\S\2015\2015.04.06 12.39"
    configfile = os.path.join(root, "server.conf")
    calfile = os.path.join(root, "server.cal")
    canfile = os.path.join(root, "can.txt")
    synchrostart = 10000
    synchrostop = 30000
    start, stop = can_txt2.lines_range(canfile, synchrostart, synchrostop)

    ca = ConfigAdapter(configfile, calfile, "Synergy")
    can = can_txt2.CanTxt(canfile, start, stop, confAdapter=ca, model="Synergy")

    lc = LevelCalc(can, ca)
    lc_ab = LevelCalcAB(can, ca)

    calc_res = lc()
    #calc_res_ab = lc_ab()
    print("done!")
