﻿# coding: utf-8
#
import os
from numpy import int32, resize, split, int64, str, pi, float32
from numpy import int, ndarray, add, diff, zeros, where

def guess_nlines_can(filename):
    """
    Оценка числа строк в can.txt
    (считаем, что средняя длина строки чуть больше 50)
    """
    #return os.path.getsize(filename) // 50
    with open(filename) as f:
        nchars = 0
        nlines = 1
        for nlines, line in enumerate(f):
            if nlines > 10:
                break
            nchars += len(line)
        avgLine = nchars / nlines
        n = os.path.getsize(filename) / avgLine
        return int32(n * 1.5)


def foo(x):
    pass


def lines_range(file_path, start, stop, onProgress=foo):
    '''
    Возвращает номера строк can.txt по номерам 208-х пакетов.
    '''
    if start is None and stop is None:
        return None, None
    if (start, stop) == (0, 0):
        try:
            with open(file_path, 'r') as f:
                nlines = 0
                for line in f:
                    nlines += 1
            return 0, nlines-1
        except:
            raise RuntimeError("Could not read file at {0}".format(file_path))

    N = guess_nlines_can(file_path)

    def is_sync(line):
        split = line.split()
        return \
            (len(split) == 2 and split[1] == '208') or \
            (len(split) > 2 and (split[1] == '208' or split[2] == '208'))

    can_start = None
    can_stop = None
    sync_cnt = -1
    try:
        with open(file_path, 'r') as f:
            for i, line in enumerate(f):
                onProgress(i*100 // N)
                if not is_sync(line):
                    continue
                sync_cnt += 1
                if start is not None and sync_cnt <= start:
                    can_start = i
                    continue
                if stop is not None and sync_cnt <= stop:
                    can_stop = i
                    continue
                break
    except:
        raise RuntimeError("Could not read file at {0}".format(file_path))

    return can_start, can_stop


def parse_datum_syn(datum, offset, count):
    """
    Функция парсит одно сообшение из can.txt
    Возвращаемый результат представляет собой сырые показания датчика в виде массива чисел.

    offset -- отступ от начала сообщения, до первого байта, с которого начинаются данные датчика
    count -- количество байт в данных с датчика

    Пример строки из can.txt:

    11:52:14.265: 11952476  289 CF 00 00 E5 02 47 17 00
                                |      |
                                |      offset=0, count=3
                                |
                                Первый байт данных (offset = 0) (SIC!!!)
    """
    x = 0
    for i in range(count):
        x |= int(datum[i + offset], 16) << (8 * i)
    return x


def parse_datum_7374(datum, i):
    # frames = [msg.data[1:] for msg in self.get_messages(node_id, 8)]
    f = [int(d, 16) for d in datum][1:]
    sensors = zeros(4)
    sensors[0] = f[0] | ((f[1] & 0x0F) << 8)
    sensors[1] = (f[1] >> 4) | (f[2] << 4)
    sensors[2] = f[3] | ((f[4] & 0x0F) << 8)
    sensors[3] = (f[4] >> 4) | (f[5] << 4)
    return sensors[i]


def parse_direction(datum, model, mask, mode):
    val = 0
    if model != "7374":
        for i, d in enumerate(datum[1:]):
            val |= int(d, 16) << (8 * (i + 1))
        val &= mask
        return 1 - val if mode & 1 == 0 else val
    else:
        return 1;

ID_T1 = 0x289
ID_T2 = 0x28A
ID_T3 = 0x28B
ID_T4 = 0x28C
ID_T5 = 0x28D
ID_GYRO = 0x1A9
ID_SYNCHRO = 0x208
ID_MTKP_T1 = 0x293
ID_MTKP_T2 = 0x296
ID_MTKP_T3 = 0x295
ID_MTKP_T4 = 0x297

LEN_LONG = 8
LEN_SHORT = 7


def read_params(filename):
    from canparams import canparams

    if filename.endswith(".xml"):
        import xml.etree.ElementTree as ET

        tree = ET.parse(filename)
        root = tree.getroot()

        canparams = {}
        for model in root:
            p = {}
            for signal in model:
                ID = eval(signal.find("id").text)
                if ID not in p.keys(): p[ID] = {}
                length = eval(signal.find("length").text)
                if length not in p[ID].keys(): p[ID][length] = {}
                try:
                    params = (
                        int(signal.find("offset").text) + 1,
                        int(signal.find("count").text))
                except AttributeError:
                    params = (
                        int(signal.find("i").text),
                    )
                p[ID][length][signal.find("label").text] = params

            canparams[model.get("name")] = p
    return canparams


class ProgressPrinter:
    def __init__(self):
        self.oldval = None

    def printRound(self, val):
        if not val == self.oldval and val % 10 == 0:
            print("{0}%".format(val))
        self.oldval = val


# @profile
def read_can(canfile, paramfile=None, model="Synergy",
             onProgress=None, WARN=False, start=0, stop=None, dirID=0x2D1,
             confAdapter=None):
    if paramfile is None:
        folder = os.path.sep.join(__file__.split(os.path.sep)[:-1])
        paramfile = os.path.join(folder, 'canparams.py')
    try:
        params = read_params(paramfile)[model]
    except TypeError:
        raise RuntimeError(
            "Could not read params from file {0} for model {1}".format(
                paramfile, model))

    if confAdapter is not None:
        dirID = confAdapter.dir_event()[0]
        dirmask = confAdapter.dir_event()[1]
        dirmode = confAdapter.dir_event()[2]

    parse_datum = parse_datum_7374 if model == "7374" else parse_datum_syn
    N = guess_nlines_can(canfile)
    if stop is None:
        stop = N
    NODE_IDs = list(params.keys())
    HELPER_IDs = [0x208, dirID]
    # IDLIST.append(dirID)
    UNKNOWN_IDS = set()
    signals = {}
    t_synchro = ndarray(N, int64)
    t_gyro = ndarray(N, int64)
    direction = ndarray(N, float32)
    lastdir = 1

    t_start = None  # Начало отсчёта времени
    cnt = [0, 0]  # счётчики для массивов: cnt[0] гироскоп, cnt[1] синхропакет
    synchrobyte = 256  # первый байт (инкрементировать счётчик, только если изменился)

    for ID in NODE_IDs:
        for l in params[ID]:
            for s in params[ID][l]:
                signals[s] = ndarray(N, int32)
    with open(canfile, 'r') as F:
        lineCnt = 0
        while lineCnt < start:
            F.readline()
            lineCnt += 1

        # Пропускаем всё до первого 208 пакета
        while synchrobyte == 256 and lineCnt < stop:
            lsplit = F.readline().split()
            if len(lsplit) > 4 and lsplit[2] == "208":
                synchrobyte = int(lsplit[3], 16)
            lineCnt += 1

        for line in F:
            if not line.strip()[0].isnumeric():
                continue
            if lineCnt > stop:
                break
            if onProgress is not None:
                if lineCnt % 100 == 0:
                    onProgress(lineCnt * 100 // N)
            lineCnt += 1

            lsplit = line.split()
            if len(lsplit) < 4:
                continue

            t = int64(lsplit[1].strip(" :"))
            if t_start is None:
                t_start = t

            ID = int(lsplit[2], 16)
            datum = lsplit[3:]
            l = len(datum)

            if ID == 0x208:
                t_synchro[cnt[1]] = t - t_start
                direction[cnt[1]] = lastdir
                cnt[1] += 1
                continue

            if ID == dirID:
                lastdir = parse_direction(datum, model, dirmask, dirmode)
                continue

            if ID not in NODE_IDs and ID not in HELPER_IDs:
                UNKNOWN_IDS.add(ID)
                continue

            if ID == ID_GYRO:
                curr_cnt = 0
                parse = parse_datum_syn

            else:
                curr_cnt = 1
                parse = parse_datum

            curr_cnt = 0 if ID == ID_GYRO else 1

            if l not in (7, 8):
                continue
            try:
                for signame in params[ID][l].keys():
                    signals[signame][cnt[curr_cnt]] = parse(
                        datum, *params[ID][l][signame])
            except KeyError as wrongKey:
                continue
            except RuntimeError:
                print("could not parse datum: {0} with {1}({2})".format(datum, parse.__name__, params[ID][l][signame]))

            if ID == ID_GYRO:
                t_gyro[cnt[0]] = t - t_start
                cnt[curr_cnt] += 1
    for ID in NODE_IDs:
        i = cnt[0] if ID == 0x1A9 else cnt[1]
        for l in params[ID]:
            for s in params[ID][l]:
                # signals[s] = signals[s][:i]
                signals[s].resize(i)
    if True:#cnt[1] > len(t_synchro):
        t_synchro.resize(cnt[1])
    for i in where(diff(t_synchro) < 0)[0]:
        t_synchro[i + 1:] += 2 ** 32
    if True: #cnt[1] > len(direction):
        direction.resize(cnt[1])
    if True: #cnt[0] > len(t_gyro):
        t_gyro.resize(cnt[0])
    for i in where(diff(t_gyro) < 0)[0]:
        t_gyro[i + 1:] += 2 ** 32

    signals["t_synchro"] = t_synchro
    signals["t_gyro"] = t_gyro
    signals["dircoeff"] = direction
    if len(UNKNOWN_IDS) > 0 and WARN is True:
        print("CAN parser encountered unknown node IDs:", ",".join(map(lambda x: "0x{0:X}".format(x),
                                                                       UNKNOWN_IDS)))

    return signals


class CanParams:
    def __init__(self):
        self.dl = 0.1

    def get_dl(self):
        return self.dl


cp = CanParams()


def normlz(raw, zero, bits):
    base = 1 << (bits - 1)
    mask = base * 2 - 1
    a = base - raw
    b = (zero + a) & mask
    return b - base


class CanTxt:
    def __init__(self, filename, start=0, stop=None, model="Synergy", confAdapter=None,
                 onProgress=None):
        rawcan = read_can(filename, model=model, start=start, stop=stop, confAdapter=confAdapter,
                          onProgress=onProgress)
        self.__dict__.update({"gyro": {},  # [[]]*3,
                              "acs": {},  # [[]]*3,
                              "y": {},  # [[]]*4,
                              "z": {},  # [[]]*8,
                              "s": {}  # [[]]*3
                              })
        for key in rawcan:
            if not key[-1].isnumeric():
                self.__dict__[key] = rawcan[key]
            else:
                shortkey = key[:-1]
                keynumber = int(key[-1])
                if shortkey not in self.__dict__.keys():
                    self.__dict__[shortkey] = {}
                self.__dict__[shortkey][keynumber] = rawcan[key]


class CanTxtSyn1(CanTxt):
    def __init__(self, filename, start=0, stop=None):
        super(CanTxtSyn1, self).__init__(filename, model="Synergy", start=start, stop=stop)


class CanSynPlain:
    def __init__(self, filename, start=0, stop=None):
        self.__dict__.update(read_can(filename, model="Synergy", start=start, stop=stop))


if __name__ == "__main__":
    can = read_can("parsing/can.txt", model="Synergy")
    print(can)
