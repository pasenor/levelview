def win_diff(x, win):
    '''
    Вычисляет производную массива *x* по окну *win*.
    Возвращает массив того же размера, что и оригинал, с центрированными индексами.
    '''
    if win < 1:  # хрень
        raise ValueError('Окно {0}'.format(win))

    from numpy import zeros
    dx = zeros(len(x))

    if win == 1:  # равносильно простому diff
        from numpy import diff
        dx[1:] = diff(x)
    else:
        dx[win // 2:-win // 2] = x[win:] - x[:-win]

    return dx

