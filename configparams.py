configparams = {
        "Sever": {
        "lir_bits": [13] * 16,
        "lir_radius": [35] * 16,
        "inds": {
            'z': list(range(8, 16)),
            'acs': 31,
            'gyr_angle': 32,
            'gyr_sm': 33,
            'speed_corr': 34,
            'hor_gyro': 37,
            'speed': 30},
        "trolley_for_level": 1,
    },

        "MTKP": {
        "lir_bits": [13] * 16,
        "lir_radius": [35] * 16,
        "inds": {
            'z': list(range(8, 16)),
            'acs': 31,
            'gyr_angle': 32,
            'gyr_sm': 33,
            'speed_corr': 34,
            'hor_gyro': 37,
            'speed': 30},
        "trolley_for_level": 1,
    },


    "Synergy": {
        "lir_bits": [13] * 16,
        "lir_radius": [35] * 16,
        "inds": {
            'z': list(range(8, 16)),
            'acs': 31,
            'gyr_angle': 32,
            'gyr_sm': 33,
            'speed_corr': 34,
            'hor_gyro': 37,
            'speed': 30},
        "trolley_for_level": 1,
    },
    "Integral": {
        "lir_bits": [13] * 16,
        "lir_radius": [35] * 16,
        "inds": {
            'z': list(range(8, 16)),
            'acs': 31,
            'gyr_angle': 32,
            'gyr_sm': 33,
            'speed_corr': 34,
            'hor_gyro': 37,
            'speed': 30},
        "trolley_for_level": 1,
    },

    "7374": {
        "lir_bits": [12] * 16,
        "lir_radius": [35] * 16,
        "inds": {'z': list(range(8, 16)),
                 'acs': 31,
                 'gyr_angle': 32,
                 'gyr_sm': 33,
                 'speed_corr': 34,
                 'speed': 30},
        "trolley_for_level": 1,
    }}