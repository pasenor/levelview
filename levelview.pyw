# coding: utf-8

from PyQt4 import QtGui, QtCore
import sys
import random
import pyqtgraph as pg
from LevelLoader import LevelLoader
from panel import LevelControlPanel, DummyPanel
from numpy import arange, isinf, nan, searchsorted
from scipy.interpolate import interp1d

pg.setConfigOption("background", "w")


class ViewerWindow(QtGui.QMainWindow):
    def __init__(self):
        super(ViewerWindow, self).__init__()
        self.setWindowTitle("Просмотр уровня")
        menu = QtGui.QMenuBar()
        importMenu = menu.addMenu("Импорт")
        importXmlAction = importMenu.addAction("Открыть файл сравнения")
        importXmlAction.setShortcut(QtGui.QKeySequence("Ctrl+O"))
        importProezdAction = importMenu.addAction("Добавить проезд")
        importProezdAction.setShortcut(QtGui.QKeySequence("Ctrl+A"))

        self.connect(importXmlAction, QtCore.SIGNAL("triggered()"),
                     self.fileOpenCompar)
        self.connect(importProezdAction, QtCore.SIGNAL("triggered()"),
                     self.fileOpenProezd)
        exportMenu = menu.addMenu("Экспорт")
        self.exportConfigAction = exportMenu.addAction("Экспорт конфигурации")
        self.exportConfigAction.setShortcut(QtGui.QKeySequence("Ctrl+S"))
        self.exportConfigAction.setEnabled(False)

        self.exportDataAction = exportMenu.addAction("Экспорт данных (уровень)")
        self.exportXmlAction = exportMenu.addAction("Экспорт файла сравнения")

        self.connect(self.exportConfigAction, QtCore.SIGNAL("triggered()"),
                     self.confExport)
        self.connect(self.exportDataAction, QtCore.SIGNAL("triggered()"),
                     self.dataExport)
        self.connect(self.exportXmlAction, QtCore.SIGNAL("triggered()"),
                     self.xmlExport)
        self.setMenuBar(menu)

        self.gLayout = Grafik()

        self.panelTabs = QtGui.QTabWidget()
        self.panelTabs.addTab(DummyPanel(), "Проезд")
        self.panelTabs.setMaximumWidth(700)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum,
                                       QtGui.QSizePolicy.Minimum)
        self.panelTabs.setSizePolicy(sizePolicy)

        self.panelTabs.setEnabled(False)

        container = QtGui.QWidget()
        layout = QtGui.QGridLayout()
        layout.addWidget(self.gLayout, 0, 0, 5, 3)
        layout.addWidget(self.panelTabs, 0, 3, 5, 1)
        container.setLayout(layout)
        self.setCentralWidget(container)

        self.loader = LevelLoader()
        self.curveCnt = 0
        self.signals = {}

        self.keyPressEvent = lambda event: self.gLayout.keyPressEvent(event)
        self.keyReleaseEvent = lambda event: self.gLayout.keyReleaseEvent(event)

        self.connect(self.gLayout, self.gLayout.shiftSignal,
                     self.onMouseShift)
        self.connect(self.gLayout, self.gLayout.doneShifting,
                     self.onDoneShifting)
        self.connect(self.panelTabs, QtCore.SIGNAL("currentChanged(int)"),
                     self.plotDiff)
        self.connect(self.panelTabs, QtCore.SIGNAL("currentChanged(int)"),
                     self.flashSignalLines)

    def flashSignalLines(self, i):
        # self.panelTabs.widget(i).showButton.emit(QtCore.SIGNAL("pressed()"))
        pass

    def onDoneShifting(self, dx):
        i = self.panelTabs.currentIndex()
        self.signals[i].saveShift(dx)
        self.plotDiff(i)

    def onMouseShift(self, shval):
        dx, dy = shval
        i = self.panelTabs.currentIndex()
        if self.curveCnt == 0:
            return
        self.signals[i].shift(dx, dy=dy)
        self.plotDiff(i)

    def confExport(self):
        import calc2 as calc

        filename = QtGui.QFileDialog.getSaveFileName(self, "Экспорт конфигурации",
                                                     ".",
                                                     "файлы конфигурации (*.conf);; all files")
        ca = self.signals[self.panelTabs.currentIndex()].calculator.conf
        gconf = calc.generate_config(ca)
        with open(filename, "w") as f:
            f.write(gconf)

    def xmlExport(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Экспорт файла сравнения",
                                                     ".", "файлы xml (*.xml)")
        self.loader.tree.write(filename, encoding="utf-8")

    def dataExport(self):
        filename = QtGui.QFileDialog.getSaveFileName(self, "Экспорт данных",
                                                     ".",
                                                     "текстовые файлы (*.txt)")
        i = self.panelTabs.currentIndex()
        with open(filename, "w") as f:
            for n in self.signals[i].data:
                f.write(str(n) + "\n")

    def fileOpenCompar(self, xmlfile=None):
        if xmlfile is None:
            xmlfile = QtGui.QFileDialog.getOpenFileName(self, "Открыть файл",
                                                        ".",
                                                        "xml files (*.xml);;all files(*)")
        self.loader.loadXML(xmlfile)
        calculators, levels, labels, nodes = self.loader.getLevels()
        for c, l, node, lab in zip(calculators, levels, nodes, labels):
            self.addSignalLine(c, node, lab)

    def plotDiff(self, i):
        if i not in self.signals:
            return
        x = self.signals[i].x + self.signals[i].xShift
        y = self.signals[i].data + self.signals[i].yShift
        self.signals[i].diffplot.setData([], [])
        for j in self.signals:
            if j == i:
                continue
            imin = searchsorted(self.signals[i].x + self.signals[i].xShift,
                                self.signals[j].x[0] + self.signals[j].xShift)
            imax = searchsorted(self.signals[j].x + self.signals[i].xShift,
                                self.signals[j].x[-1] + self.signals[j].xShift)
            spline = interp1d(self.signals[j].x + self.signals[j].xShift,
                              self.signals[j].data + self.signals[j].yShift,
                              bounds_error=False, fill_value=nan)
            self.signals[j].diffplot.setData(
                self.signals[i].x[imin:imax][::self.signals[i].xFlowDir] + self.signals[i].xShift,
                self.signals[i].data[imin:imax] * self.signals[i].yCoeff + self.signals[i].yShift - spline(
                    self.signals[i].x[imin:imax][::self.signals[i].xFlowDir] + self.signals[i].xShift
                ))

    def addSignalLine(self, calculator, node, label):
        if self.curveCnt == 0:
            self.panelTabs.removeTab(0)
        self.curveCnt += 1
        panel = LevelControlPanel(self.loader.labels[-1], calculator.conf)
        calcres = calculator()
        level = calcres.level
        x = calcres.coord
        signalLine = SignalLine()
        signalLine.setPlot(self.gLayout.plot.plot())
        signalLine.setDiffPlot(self.gLayout.diffplot.plot())
        signalLine.setData(level, x=x)
        i = self.panelTabs.addTab(panel, "Проезд {0}".format(self.curveCnt))
        signalLine.panel = panel
        signalLine.calculator = calculator
        signalLine.node = node
        self.signals[i] = signalLine

        self.connect(panel.showButton, QtCore.SIGNAL("pressed()"),
                     lambda: self.signals[self.panelTabs.currentIndex()].setWidth(2))
        self.connect(panel.showButton, QtCore.SIGNAL("released()"),
                     lambda: self.signals[self.panelTabs.currentIndex()].setWidth(1))
        for v in panel.controls.viewerList:
            self.connect(v, QtCore.SIGNAL("viewerUpdated"), self.recalcLevel)
        self.connect(panel, QtCore.SIGNAL("hideSignal"), self.hideSignal)
        self.connect(panel, QtCore.SIGNAL("rmSignal"), self.rmSignal)
        self.connect(panel, QtCore.SIGNAL("flipDirSignal"), self.flipDir)
        self.connect(panel, QtCore.SIGNAL("flipCoordSignal"), self.flipX)
        self.connect(panel, QtCore.SIGNAL("flipSignalSignal"), self.flipY)

        self.gLayout.plot.disableAutoRange()
        self.panelTabs.setEnabled(True)
        self.exportConfigAction.setEnabled(True)

    def flipDir(self):
        i = self.panelTabs.currentIndex()
        self.signals[i].calculator.flip_direction()
        self.recalcLevel()
        self.panelTabs.widget(i).controls.viewerList[0].setValue(
            self.panelTabs.widget(i).controls.viewerList[0].getValue()
        )

    def flipX(self):
        i = self.panelTabs.currentIndex()
        self.signals[i].xFlowDir = -self.signals[i].xFlowDir
        self.recalcLevel()

    def flipY(self):
        i = self.panelTabs.currentIndex()
        self.signals[i].yCoeff = -self.signals[i].yCoeff
        self.recalcLevel()

    def hideSignal(self):
        i = self.panelTabs.currentIndex()
        if self.signals[i].plot.isVisible():
            self.signals[i].plot.hide()
            self.signals[i].diffplot.hide()
        else:
            self.signals[i].plot.show()
            self.signals[i].diffplot.hide()

    def rmSignal(self):
        i = self.panelTabs.currentIndex()
        self.loader.tree.getroot().remove(self.signals[i].node)
        self.signals[i].plot.setParent(None)
        self.signals[i].diffplot.setParent(None)
        self.signals[i].panel.setParent(None)

        self.signals.pop(i)

    def fileOpenProezd(self):
        from LevelFileSelector import CanTxtSelector, IndexSelector

        cs = CanTxtSelector()
        if cs.exec_():
            calculator, node = self.loader.addProezd(dict(
                cantxt=cs.canfile,
                conf=cs.configfile,
                cal=cs.calfile,
                label=cs.description,
                model=cs.getModel(),
                algorithm=cs.getAlgorithm(),
                start=cs.start,
                stop=cs.stop,
            ))
            self.addSignalLine(calculator, node, self.loader.labels[-1])

    def recalcLevel(self):
        if not self.panelTabs.currentWidget().synchroCheckBox.isChecked():
            ind = [self.panelTabs.currentIndex()]
        else:
            ind = [i for i in range(self.panelTabs.count())]
        slist = [self.signals[i] for i in ind]
        clist = [s.calculator for s in slist]
        sender = self.sender()
        try:
            viewer = sender
            name = "_" + viewer.name
            for c in clist:
                if viewer.index is None:
                    c.conf.__dict__[name] = viewer.getValue()
                else:
                    try:
                        c.conf.__dict__[name][viewer.index] = viewer.getValue()
                    except:
                        c.conf.__dict__[name] = {viewer.index: viewer.getValue()}
        except AttributeError:
            pass
        for s, c in zip(slist, clist):
            calcres = c()
            s.setData(calcres.level, x=calcres.coord)
            self.plotDiff(self.panelTabs.currentIndex())


class RandomColor:
    fixedColors = [
        (0, 0, 0),
        (170, 0, 0),
        (0, 170, 0),
        (170, 170, 0),
        (0, 0, 255),
        (0, 85, 255),
        (255, 0, 0),
        (255, 170, 0),
        (255, 170, 255),
        (170, 0, 127)]
    ID = 0

    def __init__(self):
        pass
        self.__class__.ID += 1

    def get(self):
        if self.ID < len(self.fixedColors):
            color = QtGui.QColor(*self.fixedColors[self.ID])
        else:
            r = random.randint(0, 255)
            g = random.randint(0, 255)
            b = random.randint(0, 255)
            color = QtGui.QColor(r, g, b)
        self.ID += 1
        return color


class Grafik(pg.GraphicsLayoutWidget):
    def __init__(self):
        super(Grafik, self).__init__()
        self.wheelEvent = lambda event: event.ignore()
        self.dragEnabled = False
        self.defaultMousePressEvent = self.mousePressEvent
        self.defaultMouseReleaseEvent = self.mouseReleaseEvent
        self.defaultMouseMoveEvent = self.mouseMoveEvent
        self.setMouseTracking(False)
        self.mouseTracking = False

        self.plot = self.addPlot()
        self.plot.showGrid(x=True, y=True)
        self.nextRow()
        self.diffplot = self.addPlot()
        self.diffplot.setFixedHeight(self.height() * 0.3)
        self.diffplot.setXLink(self.plot)
        self.diffplot.hideAxis("bottom")

        self.xref = None
        self.xshift = 0

        self.yref = None
        self.yshift = 0

        self.shiftSignal = QtCore.SIGNAL("shift")
        self.doneShifting = QtCore.SIGNAL("doneShifting")

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Shift:
            self.dragEnabled = True
            self.mousePressEvent = self.dragMousePressEvent
            self.mouseReleaseEvent = self.dragMouseReleaseEvent
            self.mouseMoveEvent = self.dragMouseMoveEvent

    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Shift:
            self.dragEnabled = False
            self.mousePressEvent = self.defaultMousePressEvent
            self.mouseReleaseEvent = self.defaultMouseReleaseEvent
            self.mouseMoveEvent = self.defaultMouseMoveEvent

    def dragMouseMoveEvent(self, event):
        if self.mouseTracking:
            vb = self.plot.vb
            pos = event.pos()
            canvasPoint = vb.mapSceneToView(pos)
            self.emit(self.shiftSignal,
                      (canvasPoint.x() - self.xref, canvasPoint.y() - self.yref))

    def dragMousePressEvent(self, event):
        self.xref = self.plot.vb.mapSceneToView(event.pos()).x()
        self.yref = self.plot.vb.mapSceneToView(event.pos()).y()
        self.mouseTracking = True

    def dragMouseReleaseEvent(self, event):
        if self.mouseTracking:
            vb = self.plot.vb
            pos = event.pos()
            canvasPoint = vb.mapSceneToView(pos)
            self.emit(self.doneShifting,
                      (canvasPoint.x() - self.xref, canvasPoint.y() - self.yref))
        self.mouseTracking = False



class SignalLine:
    colorGetter = RandomColor()

    def __init__(self):
        self.data = None
        self.x = None
        self.xShift = 0
        self.yShift = 0
        self.plot = None
        self.diffplot = None
        self.color = self.colorGetter.get()
        self.panel = None
        self.width = 1
        self.calculator = None
        self.params = None
        self.node = None
        self.xFlowDir = 1 # if -1, reverse x on the plot
        self.yCoeff = 1

    def setPlot(self, p):
        self.plot = p
        self.plot.setPen(color=self.color, width=self.width)

    def setDiffPlot(self, p):
        self.diffplot = p
        self.diffplot.setPen(color=self.color, width=self.width)

    def setData(self, data, x=None):
        data[isinf(data)] = nan
        self.data = data
        self.x = arange(len(data)) if x is None else x
        self.plot.setData(x=self.x[::self.xFlowDir] + self.xShift,
                          y=data * self.yCoeff + self.yShift)
        self.plot.emit(QtCore.SIGNAL("sigPlotChanged"))
        QtGui.QApplication.processEvents()

    def setWidth(self, width):
        self.plot.setPen(color=self.color, width=width)
        self.diffplot.setPen(color=self.color, width=width)

    def shift(self, dx, dy=0):
        if self.data is None or self.x is None:
            return
        self.plot.setData(x=self.x[::self.xFlowDir] + self.xShift + dx,
                          y=self.data * self.yCoeff + self.yShift + dy)

    def saveShift(self, shval):
        dx, dy = shval
        self.xShift += dx
        self.yShift += dy

class FileOpenBar(QtGui.QWidget):
    def __init__(self):
        pass

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    w = ViewerWindow()
    w.showMaximized()
    if len(sys.argv) > 1 and sys.argv[1].endswith(".xml"):
        w.fileOpenCompar(sys.argv[1])
    app.exec_()
