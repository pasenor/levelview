from PyQt4 import QtGui, QtCore

import varviewer


class VVLabel(QtGui.QWidget):
    def __init__(self, vv):
        super().__init__()
        self.varviewer = vv
        self.textlabel = vv.label
        self.textlabel.setAlignment(QtCore.Qt.AlignLeft)
        self.textlabel.setText(self.textlabel.text() + ":")
        self.vallabel = QtGui.QLabel(str(vv.getValue()))
        self.vallabel.setAlignment(QtCore.Qt.AlignLeft)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.textlabel)
        layout.addWidget(self.vallabel)
        self.setLayout(layout)
        self.connect(self.varviewer, QtCore.SIGNAL("viewerUpdated"),
                     self.setValue)
        self.setMinimumWidth(self.textlabel.sizeHint().width() + self.vallabel.sizeHint().width())

    def sizeHint(self):
        return QtCore.QSize(self.textlabel.sizeHint().width() + self.vallabel.sizeHint().width(),
                            self.textlabel.sizeHint().height() * 2.5)

    def setValue(self, val):
        self.vallabel.setText("<b>" + str(self.varviewer.getValue()) + "</b>")


class Panel(QtGui.QWidget):
    def __init__(self, variables):
        super().__init__()

        self.viewerList = []
        for var in variables:
            name = var[0]
            # if "cmd" in name or "shift" in name:
            #     datatype = int
            # else:
            #     datatype = float
            datatype = float
            v = varviewer.VariableViewer(name=var[0], labelText=var[1],
                                         varRange=var[2], value=var[3], direction="vertical", noname=True,
                                         datatype=datatype)
            if v.name in ("alpha", "beta"):
                v.setLogScale(True)
            self.viewerList.append(v)
            try:
                self.viewerList[-1].index = var[4]
            except IndexError:
                pass
        vvlabels = [VVLabel(v) for v in self.viewerList]
        listWidget = QtGui.QListWidget()

        viewerStack = QtGui.QStackedWidget()

        for lab in vvlabels:
            listWidgetItem = QtGui.QListWidgetItem(listWidget)
            listWidgetItem.setSizeHint(lab.sizeHint())
            listWidget.addItem(listWidgetItem)
            listWidget.setItemWidget(listWidgetItem, lab)
            viewerStack.addWidget(lab.varviewer)

        listWidget.setCurrentRow(0)
        viewerStack.setCurrentIndex(listWidget.currentRow())

        viewerStack.setFixedWidth(viewerStack.currentWidget().width())

        self.connect(listWidget, QtCore.SIGNAL("currentRowChanged(int)"),
                     viewerStack.setCurrentIndex)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(listWidget)
        layout.addWidget(viewerStack)
        self.setLayout(layout)
        # self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)


class LevelControlPanel(QtGui.QWidget):
    def __init__(self, name, conf_adapter):
        super(LevelControlPanel, self).__init__()
        namelabel = QtGui.QLabel(name)
        namelabel.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse |
                                          QtCore.Qt.TextSelectableByKeyboard)

        self.showButton = QtGui.QPushButton("Показать на графике")
        self.showButton.setFixedWidth(self.showButton.sizeHint().width())

        self.synchroCheckBox = QtGui.QCheckBox("Синхронизировать параметры")
        self.synchroCheckBox.setChecked(False)

        self.flipDirButton = QtGui.QPushButton("Сменить направление")
        self.connect(self.flipDirButton, QtCore.SIGNAL("clicked()"),
                     self.flipDir)

        self.closeBtn = QtGui.QToolButton()
        self.closeBtn.setText("удалить")
        self.hideBtn = QtGui.QToolButton()
        self.hideBtn.setText("скрыть")
        self.flipCoordBtn = QtGui.QPushButton("отразить координату")
        self.flipSignalBtn = QtGui.QPushButton("отразить сигнал")

        self.connect(self.closeBtn, QtCore.SIGNAL("clicked()"),
                     self.rmProezd)
        self.connect(self.hideBtn, QtCore.SIGNAL("clicked()"),
                     self.toggleVisible)
        self.connect(self.flipCoordBtn, QtCore.SIGNAL("clicked()"),
                     self.flipCoord)
        self.connect(self.flipSignalBtn, QtCore.SIGNAL("clicked()"),
                     self.flipSignal)

        variables = [
            ("kdz", "kdz", (-1.5, 1.5), conf_adapter.kdz()),
            ("dl", "шаг координаты", (0.06, 0.15), conf_adapter.dl()),
            ("acs_coeff", "Коэффициент акселерометра", (-1.5, 1.5), conf_adapter.acs_coeff()),
            ("hi_freq_coeff", "Коэффициент гироскопа", (-1.5, 1.5), conf_adapter.hi_freq_coeff()),
            ("speed_corr_coeff", "Коэффициент скоростной поправки", (-1.5, 1.5), conf_adapter.speed_corr_coeff()),
            ("acs_smd", "Фильтр акселерометра", (0, 1000), conf_adapter.acs_smd()),
            ("acs_shift", "Сдвиг акселерометра", (-100, 100), conf_adapter.acs_shift()),
            ("speed_corr_shift", "Сдвиг скоростной поправки", (-100, 100), conf_adapter.speed_corr_shift()),
            ("speed_corr_smd", "Фильтр скоростной поправки", (0, 1000), conf_adapter.speed_corr_smd()),
            ("speed_shift", "Сдвиг скорости", (-100, 100), conf_adapter.speed_shift()),
            ("gyr_angle_smd", "Фильтр гироскопа 1", (0, 1000), conf_adapter.gyr_angle_smd()),
            ("gyr_sm_smd", "Фильтр гироскопа 2", (0, 1000), conf_adapter.gyr_sm_smd()),
            ("gyro_shift", "Сдвиг гироскопа", (-100, 100), conf_adapter.gyro_shift()),
            ("z_smd", "Фильтр просадочных датчиков", (0, 1000), conf_adapter.z_smd(1), 1),
            ("z_shift", "Сдвиг просадочных датчиков", (-200, 200), conf_adapter.z_shift()),
            ("alpha", "alpha", (0, 0.8), conf_adapter.alpha()),
            ("beta", "beta", (0, 0.8), conf_adapter.beta()),
            #("speed_coeff", "Коэффициент скорости", (-1.5, 1.5), conf_adapter.speed_coeff()),
        ]

        self.controls = Panel(variables=variables)

        headerlayout = QtGui.QGridLayout()
        headerlayout.addWidget(self.showButton, 0, 0, 1, 1)
        headerlayout.addWidget(self.closeBtn, 0, 2, 1, 1)
        headerlayout.addWidget(self.hideBtn, 0, 3, 1, 1)
        headerlayout.addWidget(self.synchroCheckBox, 1, 0, 1, 2)
        headerlayout.addWidget(self.flipDirButton, 1, 2, 1, 2)
        headerlayout.addWidget(self.flipCoordBtn, 2, 0, 1, 1)
        headerlayout.addWidget(self.flipSignalBtn, 2, 1, 1, 1)

        layout = QtGui.QVBoxLayout()
        # layout.addWidget(self.closeBtn)
        layout.addWidget(namelabel)

        layout.addLayout(headerlayout)
        layout.addWidget(self.controls)
        self.setLayout(layout)


    def rmProezd(self):
        self.emit(QtCore.SIGNAL("rmSignal"))

    def flipDir(self):
        self.emit(QtCore.SIGNAL("flipDirSignal"))

    def flipCoord(self):
        self.emit(QtCore.SIGNAL("flipCoordSignal"))

    def flipSignal(self):
        self.emit(QtCore.SIGNAL("flipSignalSignal"))

    def toggleVisible(self):
        self.hideBtn.setText("скрыть" if self.hideBtn.text() == "показать" else "показать")
        self.emit(QtCore.SIGNAL("hideSignal"))

class DummyPanel(QtGui.QWidget):
    def __init__(self):
        super(DummyPanel, self).__init__()
        namelabel = QtGui.QLabel("Параметры расчёта уровня...")
        layout = QtGui.QHBoxLayout()
        layout.setAlignment(QtCore.Qt.AlignTop)
        layout.addWidget(namelabel)
        self.setLayout(layout)
