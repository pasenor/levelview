# coding: utf-8
import re
import codecs
import time
from bunch import PyrailBunch as Bunch

def _parse(value):

    items = value.split(',')

    if len(items) == 0:
        return value

    hex_regex = re.compile('^[+\\-]?0x[0-9A-Fa-f]+$')
    int_regex = re.compile('^[+\\-]?[0-9]+$')
    flt_regex = re.compile('^[+\\-]?(?:(?:(?:[0-9]+\.[0-9]*)|(?:[0-9]*\.[0-9]+))|[0-9]+)(?:[eE][+\\-]?[0-9]+)?$')

    result = []
    for item in items:
        if hex_regex.search(item) is not None:
            result.append(int(item, 16))
        elif int_regex.search(item) is not None:
            result.append(int(item))
        elif flt_regex.search(item) is not None:
            result.append(float(item))

    if len(result) == 0:
        return value

    if len(result) == 1:
        return result[0]

    from numpy import array
    return array(result)


def _is_header(line):
    return re.search('\\d{2}\\.\\d{2}.\\d{4} \\d{2}\\:\\d{2}:\\d{2}', line) is not None


def _tell_last_header_line(file_path):
    pos = None

    with open(file_path, 'r') as f:
        for i, line in enumerate(f):
            if _is_header(line):
                pos = i
    return pos


def load_server_conf(file_path, key_mode=None, encoding='windows-1251'):

    sections = Bunch()
    current_section = None

    if key_mode == 'lower':
        key_transform = lambda s: s.lower()
    elif key_mode == 'upper':
        key_transform = lambda s: s.upper()
    else:
        key_transform = lambda s: s
    try:
        with codecs.open(file_path, mode='r', encoding=encoding) as f:
            eqn = -1
            for line in f:
                if line.startswith(';'):
                    continue
                if '[' in line:
                    current_section = key_transform(line.split('[')[1].split(']')[0])
                    sections[current_section] = Bunch()
                    continue
                eqn = line.find('=')
                if eqn != -1:
                    k = line[:eqn].strip()
                    v = line[eqn + 1:].strip()
                    sections[current_section][key_transform(k)] = _parse(v)
        return sections
    except UnicodeDecodeError:
        with codecs.open(file_path, mode='r', encoding="utf-8") as f:
            eqn = -1
            for line in f:
                if line.startswith(';'):
                    continue
                if '[' in line:
                    current_section = key_transform(line.split('[')[1].split(']')[0])
                    sections[current_section] = Bunch()
                    continue
                eqn = line.find('=')
                if eqn != -1:
                    k = line[:eqn].strip()
                    v = line[eqn + 1:].strip()
                    sections[current_section][key_transform(k)] = _parse(v)
        return sections


def load_server_cal(file_path, header=None, key_mode=None):
    if key_mode == 'lower':
        key_transform = lambda s: s.lower()
    elif key_mode == 'upper':
        key_transform = lambda s: s.upper()
    else:
        key_transform = lambda s: s

    from itertools import dropwhile, takewhile

    if header is None:
        start = _tell_last_header_line(file_path)
        if start is None:
            raise ValueError('секция калибровки не найдена')
        with open(file_path, 'r') as f:
            lines = [line for i, line in dropwhile(lambda x: x[0] < start, enumerate(f))]
    else:
        with open(file_path, 'r') as f:
            lines = [line for line in dropwhile(lambda line: line.find(header) < 0, f)]
            if len(lines) == 0:
                raise ValueError('секция калибровки не найдена')

    lines = [line for i, line in takewhile(lambda x: x[0] == 0 or not _is_header(x[1]), enumerate(lines))]

    items = {'_HEAD_': lines[0]}

    key_value = re.compile(r'^(?P<key>.+)=(?P<value>.+)$')

    for line in lines[1:]:
        match = key_value.search(line)
        if match is not None:
            k = match.groupdict()['key']
            v = match.groupdict()['value']
            items[key_transform(k)] = _parse(v)

    return Bunch(**items)


def load_server_cal_all(file_path):
    key_value = re.compile(r'^(?P<key>.+)=(?P<value>.+)$')
    result = []
    with open(file_path, 'r') as f:
        section = None
        for line in f:
            if _is_header(line):
                date_str = line.split('***')[1].strip()
                date = time.strptime(date_str, '%d.%m.%Y %H:%M:%S')
                section = {'__DATE__': date}
                result.append(section)
            elif section is not None:
                match = key_value.search(line)
                if match is not None:
                    k = match.groupdict()['key']
                    v = match.groupdict()['value']
                    section[k] = _parse(v)
    return result
