from distutils.core import setup
import py2exe
setup(
    windows=[{"script":"levelview.py"}], 
    options={"py2exe":
                 {"includes":["sip"],
                  "dist_dir":"../"}}, 
    requires=['numpy', 'scipy',
              'pyqtgraph', 'PyQt4'])


