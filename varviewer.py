﻿# coding: utf-8
from PyQt4 import QtGui
from PyQt4 import QtCore
from numpy import log10, log, exp


def scale(x, minval, maxval):
    def logc(A):
        res = (1 - 0.5 * log(A) / (log(A)-log(2)))
        res /= (1/log(A) - 1/(log(A)-log(2)))
        return res
    xx = x - minval
    xmax = maxval-minval
    lc = logc(xmax)
    k = 1-lc/log(xmax)
    c = exp(lc)
    return c * xx ** k + minval


class VariableViewer(QtGui.QWidget):
    counter = 0
    labelWidth = 0
    sliderWidth = 0
    spinboxWidth = 0

    def __init__(self, name=None, datatype=float,
                 labelText=None, varRange=(0, 100),
                 measSuffix="", value=50,
                 direction="horizontal", noname=False,
                 logscale="False"):
        self.logscale = logscale
        self.__class__.counter += 1
        super(VariableViewer, self).__init__()
        if labelText is None:
            labelText = "Var {0}".format(self.counter)
        self.name = name if name is not None \
            else "viewer {0}".format(self.counter)

        self._value = value
        self.index = None # Если value -- вектор. т.е. вычисляется как config_adapter.some_value(i)
                          # то index = i
        valmin, valmax = varRange
        if self._value < valmin or self._value > valmax:
            raise RuntimeError("varviewer {0}: value {1} outside "
                               "the specified range {2}"
                               .format(name, self._value, varRange))
        self.default = self._value if self._value is not None else 0.5 * (valmin + valmax)
        self.valcoeff = 100 if datatype == float else 1

        self.label = QtGui.QLabel(labelText)
        self.label.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
        newLabelWidth = self.label.sizeHint().width()
        if newLabelWidth > self.__class__.labelWidth:
            self.__class__.labelWidth = newLabelWidth
        dirdict = {"horizontal": QtCore.Qt.Horizontal, "vertical": QtCore.Qt.Vertical}
        self.slider = QtGui.QSlider(dirdict[direction])
        self.slidermin, self.slidermax = valmin * self.valcoeff, valmax * self.valcoeff
        self.slider.setRange(self.slidermin, self.slidermax)
        self.slider.setValue(self.default * self.valcoeff)
        self.slider.setTickInterval(3)
        newSliderWidth = self.slider.sizeHint().width() * 3
        if newSliderWidth > self.__class__.sliderWidth:
            self.__class__.sliderWidth = newSliderWidth

        self.spinbox = QtGui.QDoubleSpinBox() if datatype == float else QtGui.QSpinBox()
        if datatype is float:
            self.spinbox.setSingleStep(self.getValue() * 0.01)
            self.spinbox.setDecimals(abs(log10(self.spinbox.singleStep()))+2)
        #self.spinbox.setFixedWidth(self.spinbox.sizeHint().width() * 2)
        self.spinbox.setSuffix(measSuffix)
        self.spinbox.setRange(valmin, valmax)
        self.spinbox.setValue(self.default)
        newSpinboxWidth = self.spinbox.sizeHint().width()
        if newSpinboxWidth > self.__class__.spinboxWidth:
            self.__class__.spinboxWidth = newSpinboxWidth

        self.resetBtn = QtGui.QPushButton("Сбросить")
        self.resetBtn.setFixedSize(self.resetBtn.sizeHint())

        self.flipBtn = QtGui.QPushButton("-x")
        self.flipBtn.setFixedWidth(25)
        if varRange[0] != -varRange[1]:
            self.flipBtn.setEnabled(False)
        if direction == "horizontal":
            layout = QtGui.QHBoxLayout()
        else:
            layout = QtGui.QVBoxLayout()
            # layout.setAlignment(QtCore.Qt.AlignHCenter)

        align = QtCore.Qt.AlignHCenter
        if not noname:
            layout.addWidget(self.label, alignment=align)
        layout.addWidget(self.slider, alignment=align)
        layout.addWidget(self.spinbox, alignment=align)
        layout.addWidget(self.resetBtn, alignment=align)
        layout.addWidget(self.flipBtn, alignment=align)

        self.setLayout(layout)
        if direction == "vertical":
            self.setFixedWidth(self.sizeHint().width())

        self.connect(self.slider, QtCore.SIGNAL("valueChanged(int)"),
                     self.setValue)
                     #lambda val: self.setValue(self.__val_from_slider__(val)))
        self.connect(self.spinbox, QtCore.SIGNAL("valueChanged(double)"),
                     self.setValue)
        self.connect(self.resetBtn, QtCore.SIGNAL("clicked()"),
                     self.reset)
        self.connect(self.flipBtn, QtCore.SIGNAL("clicked()"), self.flip)

    def __val_from_slider__(self, val):
        if self.logscale is True: # Если просто "if self.logscale:",
                                  # то почему-то всегда возвращает True
            return scale(val, self.slidermin, self.slidermax) / self.valcoeff
        else:
            return val / self.valcoeff

    def flip(self):
        val = self.spinbox.value()
        self.spinbox.setValue(-val)
        self.slider.setValue(int(-val * self.valcoeff))
        self.emit(QtCore.SIGNAL("viewerUpdated"), self.name)

    def adjustGeometry(self):
        self.label.setFixedWidth(self.__class__.labelWidth)
        self.slider.setFixedWidth(self.__class__.sliderWidth)
        self.spinbox.setFixedWidth(self.__class__.spinboxWidth)

    def reset(self):
        self.slider.setValue(int(self.default * self.valcoeff))
        self.spinbox.setValue(self.default)
        self.emit(QtCore.SIGNAL("viewerUpdated"), self)

    def getValue(self):
        return self._value

    def setValue(self, val):
        if self.sender() is self.slider:
            val = self.__val_from_slider__(val)
        self._value = val
        if self.focusWidget() is self.slider:
            self.spinbox.setValue(self._value)
        elif self.focusWidget() is self.spinbox:
            self.slider.setValue(int(val * self.valcoeff))
        self.emit(QtCore.SIGNAL("viewerUpdated"), self)

    def setLogScale(self, val=True):
        self.logscale = val